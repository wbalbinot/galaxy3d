﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Genesis : MonoBehaviour {

    float sunSize = 1;
    float starSize;
    float starDistance;

	// Use this for initialization
	void Start () {

        //* Start the Solar System
        
        // Star name / Radius Size / Distance to the sun

        //Sun / 695,508 km / 0
        CreateStar("Sun", sunSize, new Vector3(0,0,0));

        //Earth / 6,371 km / 149.6 million km
        starSize = 6.371f / 695.508f;
        starDistance = 695.508f / 149.6f;
        CreateStar("Earth", 1, new Vector3(149.6f, 0, 0));

        //** End the solar system
    }

    // Update is called once per frame
    void Update () {
		
	}

    void CreateStar(string name, float size, Vector3 position) {
        GameObject star = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        star.gameObject.name = name;

        star.transform.position = position;
        star.transform.localScale = new Vector3(size, size, size);
    }

}
